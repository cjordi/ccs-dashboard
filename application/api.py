import os
from .models import db, User
from . import results as Results
from flask_login import login_required, current_user
from werkzeug.utils import secure_filename
from flask import Blueprint, render_template, request, redirect, url_for, flash, current_app, jsonify


api = Blueprint('api', __name__)


@api.route("/file/<uid>", methods=["POST"])
@login_required
def upload_file(uid: int):
    if 'file' not in request.files:
        return jsonify({
            "valid": False,
            "info": "no files"
        })
    file = request.files['file']

    _, file_extension = os.path.splitext(file.filename)

    if file_extension != ".mat" and file_extension != ".zip":
        return jsonify({
            "valid": False,
            "info": "wrong format"
        })
    filename = secure_filename(uid + file_extension)
    file.save(os.path.join(current_app.config['UPLOAD_FOLDER'], filename))
    return jsonify({
        "valid": True,
        "file": {
            "ext": file_extension,
            "meta": "none",
            "filename": filename
        }
    })


@api.route("/getname/<dbid>", methods=["GET"])
def get_name(dbid: int):
    return Results.get_name(dbid)


@api.route("/set/<dbid>", methods=["POST"])
@login_required
def set_prop(dbid: int):
    data = request.get_json(force=True)
    prop = data.get("prop", None)
    value = data.get("value", None)
    if prop == "description":
        return Results.set_description(dbid, value)
    elif prop == "name":
        return Results.set_name(dbid, value)


@api.route("/delete/<dbid>", methods=["GET"])
@login_required
def delete(dbid: int):
    if current_user.access > 0:
        Results.delete(dbid)
    return redirect(url_for("main.index"))
