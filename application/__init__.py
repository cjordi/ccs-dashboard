from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
import os

db = SQLAlchemy()
login_manager = LoginManager()


def create_app(test_config: dict = None):
    app = Flask(__name__)
    db_path = os.path.join(os.path.dirname(__file__), 'db.sqlite')
    upload_path = os.path.join(os.path.dirname(__file__), 'static/uploads/')

    app.config['SECRET_KEY'] = 'RRESECRET0004'
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///{}'.format(db_path)
    app.config['UPLOAD_FOLDER'] = upload_path

    def _dispose_db_pool():
        with app.app_context():
            db.engine.dispose()

    try:
        from uwsgidecorators import postfork
        postfork(_dispose_db_pool)
    except ImportError:
        # Implement fallback when running outside of uwsgi...
        pass

    login_manager.login_view = 'auth.login'

    db.init_app(app)
    login_manager.init_app(app)

    from .models import User

    @login_manager.user_loader
    def load_user(user_id):
        return User.query.get(int(user_id))

    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint)

    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    from .api import api as api_blueprint
    app.register_blueprint(api_blueprint, url_prefix="/api")

    return app
