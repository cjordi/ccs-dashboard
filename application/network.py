import os
import re
import json
import pickle
import scipy.io
import requests
import polyline
import numpy as np
from glob import glob
from functools import wraps
from flask import current_app
from urllib.parse import urlencode

from . import db
from .models import Node, Edge
from . import results as Results
from .params import MAPQUESTAPI_KEY


def get(path: str):
    """
    Returns the network data

    Args:
        path (str) : Path of the .mat file

    Returns:
        nodes, edges
    """
    if ".mat" not in path:
        path += ".mat"

    struct = scipy.io.loadmat(path, squeeze_me=True)["object"]
    components = struct["component"][()]

    # Nodes
    node_names = struct["network"][()]["nodeNames"].tolist()
    nodes = [{"n": str(node), "p": {}} for node in node_names]
    for tech in components.dtype.names:
        tcomponents = np.array(components[tech][()]).tolist()
        if str(type(tcomponents)) == "<class 'tuple'>":
            tcomponents = [components[tech][()]]
        for i, component in enumerate(tcomponents):
            if component != []:
                if tech == "CCS_CO2":
                    nodes[i]["p"][tech] = component[()]["emissionscaptured"]
                else:
                    nodes[i]["p"][tech] = component[()]["size"]
                if type(nodes[i]["p"][tech]) != float and type(nodes[i]["p"][tech]) != int:
                    nodes[i]["p"][tech] = nodes[i]["p"][tech].tolist()

    # Edges
    nws = struct["analysis"][()]["sets"][()]["setNW"][()]
    if type(nws) == str:
        nws = [nws]
    else:
        nws = nws.tolist()
    network = struct["network"][()]
    start = int(struct["analysis"][()]["year"])
    horizon = int(struct["horizon"][()]["number"])

    edges = {
        "horizon": horizon,
        "year": start
    }

    info = {
        'setNW': nws
    }

    for yr in range(0, horizon):
        cyr = str(start+yr)
        edges[cyr] = []
        for nw in nws:
            if horizon == 1:
                size = network[nw][()]["size"][()]
                flow = network[nw][()]["flow"][()]
            else:
                size = network[nw][()]["size"][()][yr, :, :]
                flow = network[nw][()]["flow"][()][yr, :, :]
            rows, cols = np.nonzero(size > 100)
            edges[cyr].extend([[int(r), int(c), nw, round(flow[r, c].tolist()), round(size[r, c].tolist())]
                               for r, c in zip(rows, cols)])

    return {
        "nodes": nodes,
        "edges": edges,
        "info": info
    }


def cached(func):
    func.cache = {}
    @wraps(func)
    def wrapper(path):
        cache_path = os.path.join(path, "cache.pickle")
        isfile = os.path.isfile(cache_path)
        if isfile:
            with open(cache_path, 'rb') as handle:
                result = pickle.load(handle)
            return result
        result = func(path)
        with open(cache_path, 'wb') as handle:
            pickle.dump(result, handle, protocol=pickle.HIGHEST_PROTOCOL)
        return result
    return wrapper


@cached
def get_from_folder(fpath: str):
    """
    Load all networks from folder

    Args:
        path (str) : Path to the folder containing the results
    """
    base_path = os.path.join(fpath, "base")
    base_net = get(base_path)
    base_data = Results.load_mat(base_path)
    base_net["info"] = {
        "captured": base_data["emissionsCaptured"].tolist(),
        "total_costs": round(base_data["total_costs"])
    }
    base_net["events"] = {}
    paths = glob(os.path.join(fpath, "*.mat"))
    paths.remove(base_path+".mat")
    for path in paths:
        _, edge, step = re.search(
            r"y_(\d+)_e_(\d+)_(vuln|reco).mat", path).groups()
        evt_net = get(path)["edges"]
        evt_data = Results.load_mat(path)
        yr = str(evt_net["year"])
        if yr not in base_net["events"]:
            base_net["events"][yr] = {}
        if edge not in base_net["events"][yr]:
            base_net["events"][yr][edge] = {}
        if step not in base_net["events"][yr][edge]:
            base_net["events"][yr][edge][step] = {}
        base_net["events"][yr][edge][step]["edges"] = evt_net[yr]
        base_net["events"][yr][edge][step]["properties"] = {
            "captured": round(evt_data["emissionsCaptured"]),
            "emissions": round(evt_data["emissions"]),
            "capturedPerNode": [round(val) for val in evt_data["emissionsCapturedPerNode"] if val != np.array],
            "total_costs": round(evt_data["total_costs"])
        }
    return base_net


def load_edge(origin, target, mode, simulated=False, online_query=False):
    """
    Returns list of points or list of list of points

    Args:
        origin (str) : Name of the origin
        target (str) : Name of the target
        mode (str) : Name of the mode of transport
        simulated (bool, False): If true, the path is a straight line between the two nodes
        online_query (bool, True): If true, the path is queried online if not found in the db

    Returns:
        (list)
    """
    if simulated:
        return simulate_edge(origin, target, mode)

    edges = Edge.query.filter_by(
        origin=origin, target=target, mode=mode).all()
    if not edges:
        edges = Edge.query.filter_by(
            origin=target, target=origin, directed=0, mode=mode).all()
    if edges:
        data = []
        for edge in edges:
            data.append(edge.data())
        return data
    if online_query:
        edge_polyline = query_edge_polyline(origin, target, mode)
        if edge_polyline:
            decoded = polyline.decode(edge_polyline)
            return decoded
    return simulate_edge(origin, target, mode)


def simulate_edge(origin, target, mode):
    origin_node = Node.query.filter_by(name=origin).first()
    target_node = Node.query.filter_by(name=target).first()
    if origin_node and target_node:
        return [[[origin_node.latitude, origin_node.longitude],
                 [target_node.latitude, target_node.longitude]]]
    print("Missing:", origin, target, mode)
    return []


def query_edge_polyline(origin, target, mode):
    """
    Queries the MapQuestApi
    """
    if mode != "truck":
        return None
    print("Making request to MAPQUESTAPI")
    origin_node = Node.query.filter_by(name=origin).first()
    origin_coor = f"{origin_node.latitude},{origin_node.longitude}"
    target_node = Node.query.filter_by(name=target).first()
    target_coor = f"{target_node.latitude},{target_node.longitude}"
    query = urlencode({
        "key": MAPQUESTAPI_KEY,
        "from": origin_coor,
        "to": target_coor
    })
    rq = requests.get(
        f"""http://www.mapquestapi.com/directions/v2/route?{query}""")
    output = rq.json()
    session_id = output["route"]["sessionId"]
    query = urlencode({
        "sessionId": session_id,
        "key": key,
        "fullShape": "true"
    })
    rq = requests.get(
        f"""http://www.mapquestapi.com/directions/v2/routeshape?{query}""")
    route = rq.json()
    spoints = route["route"]["shape"]["shapePoints"]
    coords = [(spoints[i*2], spoints[i*2+1])
              for i in range(int(len(spoints)/2))]
    encoded = polyline.encode(coords)
    new_edge = Edge(origin=origin, target=target,
                    mode=mode, polyline=encoded)
    db.session.add(new_edge)
    db.session.commit()
    return encoded
