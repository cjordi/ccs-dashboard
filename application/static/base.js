document.querySelectorAll("div[data-toggle]").forEach((item) => {
	item.addEventListener(
		"click",
		({ target }) => {
			const listId = target.getAttribute("data-toggle");
			const listEl = document.querySelector(`#${listId}`);
			listEl.getAttribute("style")
				? listEl.removeAttribute("style")
				: listEl.setAttribute("style", "max-height:0px");
		},
		false
	);
});

document.querySelectorAll("div[data-switch]").forEach((item) => {
	item.addEventListener(
		"click",
		({ target }) => {
			const listId = target.getAttribute("data-switch");
			const listEl = document.querySelector(`#${listId}`);
			listEl.setAttribute(
				"data-value",
				!(listEl.getAttribute("data-value") == "true")
			);
		},
		false
	);
});
