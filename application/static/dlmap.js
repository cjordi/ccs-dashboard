const mapstyle = {
    nodes : {
        stroke : "#353535",
        minsize: 2,
        maxsize : 10,
        default : 7,
        industries: {
            WtE : "#FFAE66",
            "" : "white",
            Storage : "white"
        }
    },
    edges : {
        CO2P_GF: "#73BFBB",
        CO2S: "#7DBCE8",
        CO2T : "#4D4D4D",
        CO2R : "#D61010",
        CO2B : "#99DAFF",
        WoodT: "#f1a100",
        size_opacity: 0.4,
        minsize : 5,
        maxsize : 14
    },
    highlight : {
        light : "rgba(255,255,255,0.3)",
        dark : "rgba(0,0,0,0.3)",
        length : 5
    }
}

const projections = [
    {
        name : "Switzerland",
        center :[8.3,46.775],
        scale : 7500,
        translate : [ 400, 300]          
    },
    {
        name : "Netherlands",
        center : [4.4260191,51.9161519],
        scale : 5500,
        translate : [100,230]
    },
    {
        name : "Norway",
        center : [4.883146,60.55295],
        scale : 3000,
        translate : [ 150, 75 ]
    }
]

class Projector {
    constructor(){
        this.projections = undefined
        this.names = []
        
        this.closest = this.closest.bind(this)
        this.use = this.use.bind(this)

        this.init()
    }

    init(){
        this.projections = projections.map(this.createProjection)
        this.names = projections.map(el => el.name)
    }

    createProjection(obj){
        return d3.geoMercator()
        .center(obj.center)                
        .scale(obj.scale)                       
        .translate(obj.translate)
    }

    use(name){
        const ind = this.names.indexOf(name)
        return this.projections[ind]
    }

    closest(coord){
        const distances = projections.map(({center})=>{
            return (coord[0]-center[0])**2 + (coord[1] - center[1])**2
        })
        const ind = distances.findIndex(el=>el===Math.min(...distances))
        return this.projections[ind](coord)
    }
}


class DlMap {
	constructor({nodes,year,edges,yindex,filename}) {
        this.filename  = filename
        this.nodes = nodes
        this.edges = edges[+year]
        this.year = year
        this.yindex = yindex
        this.width = 800
        this.height = 700
        this.run()
	}

    async run(){
        this.createContainer()
        this.setProjection()
        await this.createMap()
        this.projectNodes()
        this.appendEdges()
        this.appendNodes()
        this.download()
    }

    createContainer(){
        const child = document.createElement("svg")
        child.setAttribute("id","d3map")
        child.setAttribute("xmlns","http://www.w3.org/2000/svg")
        child.setAttribute("xmlns:xlink","http://www.w3.org/1999/")
        child.setAttribute("width",`${this.width}`)
        child.setAttribute("height",`${this.height}`)
        child.setAttribute("viewBox", `0 0 ${this.width} ${this.heigth}`)

        // child.style = 'display:none;'
        const parent = document.createElement("div")
        parent.setAttribute("id","d3container")
        parent.append(child)
        document.body.prepend(parent)

        this.svg = d3.select("svg#d3map")
    }
    

    setProjection(){

        this.projector = new Projector()
        this.projection = this.projector.use("Switzerland")
       
        const _scaleNode = d3.scaleSqrt()
        .domain([0, 2e5])
        .range([mapstyle.nodes.minsize, mapstyle.nodes.maxsize])

        this.scaleNode = (val) => {
            if (val < 100 || val == undefined){
                return mapstyle.nodes.default
            }
            return _scaleNode(val)
        }

        const _scaleEdge = d3.scaleSqrt()
        .domain([0,4e6])
        .range([mapstyle.nodes.minsize, mapstyle.edges.maxsize])

        this.scaleEdge = (val) => {
            if (val < 100){
                return 0
            }
            return _scaleEdge(val)
        }
    }

    createMap(){
        const that = this;
        const promiseCH =  new Promise((res,rej)=>{
            d3.json("/static/res/maps/switzerland.geojson").then( function(data){
        
                //Rewind the coordinates 
                data = turf.rewind(data, {reverse: true, mutate: true})
                // Draw the map
                that.svg.insert("g")
                .attr("class","base")
                .selectAll("path")
                .data(data.features)
                .join("path")
                .attr("fill", "#F3F3F3")
                .attr("d", d3.geoPath()
                    .projection(that.projection)
                )
                .style("stroke", "#909090")
                res(200)
            })
        })
        const promiseNL1 = new Promise((res,rej)=>{
            d3.json("/static/res/maps/rotterdam.geojson").then( function(data){
                //Rewind the coordinates 
                data = turf.rewind(data, {reverse: true, mutate: true})
                // Draw the map
                that.svg.insert("g")
                .attr("class","base-rotterdam")
                .selectAll("path")
                .data(data.features)
                .join("path")
                .attr("fill", "#F3F3F3")
                .attr("d", d3.geoPath()
                    .projection(that.projector.use("Netherlands"))
                )
                .style("stroke", "#909090")
                res(200)
            })
        })
        const promiseNL2 = new Promise((res,rej)=>{
            d3.json("/static/res/maps/rotterdam_water.geojson").then( function(data){
                //Rewind the coordinates 
                data = turf.rewind(data, {reverse: true, mutate: true})
                // Draw the map
                that.svg.insert("g",":first-child")
                .attr("class","base-rotterdam-water")
                .selectAll("path")
                .data(data.features)
                .join("path")
                .attr("fill", "#C6EAFF")
                .attr("d", d3.geoPath()
                    .projection(that.projector.use("Netherlands"))
                )
                res(200)
            })
        }) 
        const promiseNO = new Promise((res,rej)=>{
            d3.json("/static/res/maps/norway.geojson").then( function(data){
                //Rewind the coordinates 
                data = turf.rewind(data, {reverse: true, mutate: true})
                // Draw the map
                that.svg.insert("g")
                .attr("class","base-norway")
                .selectAll("path")
                .data(data.features)
                .join("path")
                .attr("fill", "#F3F3F3")
                .attr("d", d3.geoPath()
                    .projection(that.projector.use("Norway"))
                )
                .style("stroke", "#909090")
                res(200)
            })
        })
        const promiseNO2 = new Promise((res,rej)=>{
            d3.json("/static/res/maps/norway_water.geojson").then( function(data){
                //Rewind the coordinates 
                data = turf.rewind(data, {reverse: true, mutate: true})
                // Draw the map
                that.svg.insert("g",":first-child")
                .attr("class","base-norway-water")
                .selectAll("path")
                .data(data.features)
                .join("path")
                .attr("fill", "#C6EAFF")
                .attr("d", d3.geoPath()
                    .projection(that.projector.use("Norway"))
                )
                res(200)
            })
        })
        return Promise.allSettled([promiseCH, promiseNL1, promiseNL2, promiseNO, promiseNO2])
       
    }

    projectNodes(){
        const that = this;
        this.nodes = this.nodes.map(n => {
            const proj = that.projector.closest([n.longitude, n.latitude])
            return {...n, proj}
        })
    }

    getCoord = (name)=> {
        return this.nodes.find(el => el.name === name).proj
    }

    getUnitary = ([source,target,props]) =>{
        const sc = this.getCoord(source)
        const tc = this.getCoord(target)
        const norm = normV(difV(sc,tc))
        const diff = difV(tc,sc)
        return prodV(diff,1/norm)
    }

    getStubLength(node){
        const radius = this.scaleNode(this.nodes.find(el => el.name === node).emittedCO2)
        return radius + mapstyle.highlight.length
    }

    appendEdges(){
        const that = this;

        // Size
        this.svg.append("g")
        .attr("class","edges size")
        .selectAll("line")
        .data(this.edges)
        .enter()
        .append("line")
        .attr("class","edge size")
        .attr("x1",(d)=>that.getCoord(d[0])[0])
        .attr("y1",(d)=>that.getCoord(d[0])[1])
        .attr("x2",(d)=>that.getCoord(d[1])[0])
        .attr("y2",(d)=>that.getCoord(d[1])[1])
        .style("stroke",(d) => mapstyle.edges[d[2].type])
        .style("stroke-width", (d)=>that.scaleEdge(d[2].size))
        .style("stroke-opacity",mapstyle.edges.size_opacity)
        
        // Flow
        this.svg.append("g")
        .attr("class","edges flow")
        .selectAll("line")
        .data(this.edges)
        .enter()
        .append("line")
        .attr("class","edge flow")
        .attr("x1",(d)=>that.getCoord(d[0])[0])
        .attr("y1",(d)=>that.getCoord(d[0])[1])
        .attr("x2",(d)=>that.getCoord(d[1])[0])
        .attr("y2",(d)=>that.getCoord(d[1])[1])
        .style("stroke",(d) => mapstyle.edges[d[2].type])
        .style("stroke-width", (d)=>that.scaleEdge(d[2].weight))

        // Stubs source
        const getSStub = (edge) => { // Source stub
            const u = that.getUnitary(edge)
            const source = that.getCoord(edge[0])
            const length = that.getStubLength(edge[0])
            return sumV(source, prodV(u,length))
        }
     

        this.svg.append("g")
        .attr("class","stubs source")
        .selectAll("line")
        .data(this.edges)
        .enter()
        .append("line")
        .attr("class","stub source")
        .attr("x1",(d)=>that.getCoord(d[0])[0])
        .attr("y1",(d)=>that.getCoord(d[0])[1])
        .attr("x2",(d)=>getSStub(d)[0])
        .attr("y2",(d)=>getSStub(d)[1])
        .style("stroke", mapstyle.highlight.dark)
        .style("stroke-width", (d)=>that.scaleEdge(d[2].weight))

        // Stubs target
        const getTStub = (edge) => { // Source stub
            const u = that.getUnitary(edge)
            const target = that.getCoord(edge[1])
            const length = that.getStubLength(edge[1])
            return sumV(target, prodV(u,-length))
        }
        this.svg.append("g")
        .attr("class","stubs source")
        .selectAll("line")
        .data(this.edges)
        .enter()
        .append("line")
        .attr("class","stub source")
        .attr("x1",(d)=>that.getCoord(d[1])[0])
        .attr("y1",(d)=>that.getCoord(d[1])[1])
        .attr("x2",(d)=>getTStub(d)[0])
        .attr("y2",(d)=>getTStub(d)[1])
        .style("stroke", mapstyle.highlight.light)
        .style("stroke-width", (d)=>that.scaleEdge(d[2].weight))

    }

    appendNodes(){
        const that = this;
        const getColor = (node) => {
            if (node.industry == "WtE"){
                const _o = node.properties.CCS_CO2? _get(node.properties.CCS_CO2,that.yindex)/node.emittedCO2 * 10/9 : 1 
                const _fl = (e) => Math.floor(e)
                const _c = [255, _fl(236 * (1-_o) + _o * 174), _fl(220 * (1-_o) + _o * 102)]
                return `rgb(${_c[0]},${_c[1]},${_c[2]})`
            }
            return mapstyle.nodes.industries[node.industry];
        }

        const getStrokeWidth = (node) => node.industry == "WtE"? 1: 3;
        const getStrokeColor = (node) => node.industry == ""? "#2e5eaa": mapstyle.nodes.stroke
        this.svg.append("g")
        .attr("class","nodes")
        .selectAll("circle")
        .data(this.nodes)
        .enter()
        .append("circle")
        .attr("class","node")
        .attr("r",(d) => that.scaleNode(d.emittedCO2))
        .style("fill",getColor)
        .style("stroke", getStrokeColor)
        .style("stroke-width",getStrokeWidth)
        .attr("cx",(d)=>d.proj[0])
        .attr("cy",(d)=>d.proj[1])

    }

    download(){
        const filename = this.filename
        const fakebtn = document.createElement("a")
        fakebtn.setAttribute("id","confirmdl")
        fakebtn.setAttribute("href",'data:application/octet-stream;base64,' + btoa(d3.select("div#d3container").html()))
        fakebtn.setAttribute("download",filename)
        fakebtn.style = "display:none;" 
        document.body.prepend(fakebtn)
        document.querySelector("#confirmdl").click()
        console.log("Map has been downloaded...")
        this.clean()
    }
    
    clean(){
        document.getElementById("d3container").remove()
    }

}

function _get(arr,index){
    if (typeof arr === "object" && arr.length){
        return arr[index]
    }
    return arr
}