const inputNotes = document.querySelector("#input_notes");

inputNotes.addEventListener("blur", function ({ target: { value } }) {
	fetch(`/api/set/${resultDbId}`, {
		method: "POST",
		body: JSON.stringify({
			prop: "description",
			value,
		}),
	});
});

const inputName = document.querySelector("#input_name");

inputName.addEventListener("blur", function ({ target: { innerText } }) {
	const value = innerText.replace("\n", "");
	fetch(`/api/set/${resultDbId}`, {
		method: "POST",
		body: JSON.stringify({
			prop: "name",
			value,
		}),
	});
	document.querySelector("#current_result").innerText = value;
});
