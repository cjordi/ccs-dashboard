const styling = {
	nodes: {
		max: 1.5,
		min: 0.4,
	},
	pipeline: {
		small: {
			color: "#217a79",
			weight: 1,
		},
		medium: {
			color: "#105965",
			weight: 2,
		},
		large: {
			color: "#074050",
			weight: 3,
		},
	},
	transport: {
		train: {
			color: "#a8322d",
		},
		CO2R: {
			color: "#a8322d",
		},
		truck: {
			color: "#535353",
		},
		CO2T: {
			color: "#535353",
		},
		barge: {
			color: "#60bfff",
		},
		CO2B: {
			color: "#60bfff",
		},
		ship: {
			color: "#2e5eaa",
		},
		CO2S: {
			color: "#2e5eaa",
		},
		pipeline: {
			color: "#218380",
		},
		CO2P_BF: {
			color: "#795548",
		},
		CO2P_GF: {
			color: "#218380",
		},
		default: {
			color: "#218380",
		},
		woodT: {
			color: "#f1a100",
		},
		hydrogenT: {
			color: "#3e962b",
		},
		hydrogenP: {
			color: "#218380",
		},
		hydrogenB: {
			color: "#60bfff",
		},
		hydrogenS: {
			color: "#2e5eaa",
		},
	},
	plants: {
		cement: {
			color: "#d3f2a3",
		},
		chemical: {
			color: "#97e196",
		},
		wte: {
			color: "#6cc08b",
		},
	},
	lines: {
		max: 7,
		min: 1,
	},
};

window.styling = styling;

const number_compact = (nbr) => {
	abs = Math.abs(nbr);
	if (abs > 1e9) {
		return Math.round(nbr / 1e9) + " B";
	}
	if (abs > 1e6) {
		return Math.round(nbr / 1e6) + " M";
	}
	if (abs > 1e3) {
		return Math.round(nbr / 1e3) + " k";
	}
	return Math.round(nbr);
};

    const sumV = (vec1,vec2) => [vec1[0] + vec2[0], vec1[1] + vec2[1]]
    const difV = (vec1,vec2) => [vec1[0] - vec2[0], vec1[1] - vec2[1]]
    const prodV = (vec,val) => vec.map(el=>el*val)
    const normV = (vec) => Math.sqrt(vec[0]**2 + vec[1]**2)