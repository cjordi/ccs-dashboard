class ResultMap {
	constructor(map_id, result_db_id) {
		this.result_db_id = result_db_id;
		this.map_id = map_id;
		this.map = null;
		this.tile_layer = null;
		this.nodes = null;
		this.edges = null;
		this.markers = [];
		this.polylines = {};
		this.geoJSON = null;
		this.filters = {};

		this.appendEdge = this.appendEdge.bind(this);
		this.appendNode = this.appendNode.bind(this);
		this.setFilters = this.setFilters.bind(this);
		this.toggleMode = this.toggleMode.bind(this);
		this.setModeFilter = this.setModeFilter.bind(this);
		this.clearModes = this.clearModes.bind(this);
		this.appendLegend = this.appendLegend.bind(this);
		this.loadNode = this.loadNode.bind(this);
		this.downloadMap = this.downloadMap.bind(this)
		this.getName = this.getName.bind(this)
	}

	async build() {
		this.createMap();
		this.setDefaultScaling();
		await this.fetchData();
		await this.loadNodes();
		this.setDefaultFilters();
		await this.applyFilters();
		this.appendNodes();
		this.appendLegend();
		this.startEvent();
	}

	startEvent() {
		this.dlmapListener()
	}

	dlmapListener(){
		document.getElementById("downloadmap").addEventListener("click",this.downloadMap)
	}

	getName(){
		return new Promise((res,rej)=>{
			fetch(`/api/getname/${this.result_db_id}`).then((resp)=>res(resp.text()))
		})
	}

	async downloadMap(){
		console.log("Downloading the map...")
		const name = await this.getName()
		const filename = name.replace(" ", "_") + "_map.svg"
		const dlmap = new DlMap({
			filename : filename,
			nodes:this.nodes,
			edges:this.edges,
			year : this.filters? this.filters.year : this.start_year,
			yindex : this.filters? this.filters.year - +this.start_year : 0
		})
	}

	createMap() {
		this.map = L.map(this.map_id, {
			fullscreenControl: {
				pseudoFullscreen: false,
			},
		}).setView([46.775, 7.894], 7);
		this.map.on("contextmenu", () => {});
		this.tile_layer = L.tileLayer(
			"https://{s}.tile.openstreetmap.de/{z}/{x}/{y}.png",
			{
				attribution:
					'Jordi Campos, ETHZ  <a href="https://www.openstreetmap.org/">OpenStreetMap</a>',
				minZoom: 6,
			}
		);
		this.tile_layer.addTo(this.map);
	}

	handleMapRightClick() {}

	async fetchData() {
		const networkResp = await fetch(`/network/${this.result_db_id}`);
		const network = await networkResp.json();
		const { nodes, edges, events, info } = this.formatNetwork(network);
		this.nodes = nodes;
		this.edges = edges;
		this.events = events;
		this.info = info;
		this.start_year = edges.year;
		this.horizon = edges.horizon;
	}

	formatNetwork(network) {
		network.nodes = network.nodes.map(({ n, p }) => ({
			name: n,
			properties: p,
		}));
		for (let i = 0; i < network.edges.horizon; i++) {
			const cyear = network.edges.year + i;
			network.edges[cyear] = network.edges[cyear].map((edge) => [
				network.nodes[edge[0]].name,
				network.nodes[edge[1]].name,
				{
					type: edge[2],
					weight: edge[3],
					size: edge[4],
				},
			]);
		}
		return network;
	}

	setDefaultScaling() {
		this.line_scaling = d3
			.scaleLog()
			.domain([1e4, 5e5])
			.range([window.styling.lines.min, window.styling.lines.max]);
		this.node_scaling = d3
			.scaleLog()
			.domain([1e4, 3e5])
			.range([window.styling.nodes.min, window.styling.nodes.max]);
	}

	setDefaultFilters() {
		this.filters = {
			year: this.start_year,
		};
	}

	setFilters(filters) {
		if (!filters.year) return;
		this.clearModes();
		this.clearMarkers();
		this.clearEvent();
		this.filters = { ...this.filters, ...filters };
		this.applyFilters();
	}

	clearEvent() {}

	async applyFilters() {
		this.polylines = {
			train: [],
			truck: [],
			barge: [],
			ship: [],
			pipeline: [],
			default: [],
			other: [],
		};

		this.appendEdges();
		this.appendNodes();
	}

	appendEdges() {
		this.edges[this.filters.year].forEach((edge) => {
			this.appendEdge(edge, true);
		});
	}

	async cachedFetch(url) {
		if (localStorage.getItem(url)) {
			return JSON.parse(localStorage.getItem(url));
		} else {
			const resp = await fetch(url);
			const data = await resp.json();
			try {
				localStorage.setItem(url, JSON.stringify(data));
			} catch (e) {
				console.log("The browser cache is full");
			}
			return data;
		}
	}

	async appendEdge([origin, target, info], rightClick = false) {
		const self = this;
		const mode = this.formatTransportMode(info.type);
		const query = new URLSearchParams({ origin, target, mode }).toString();
		const points = await this.cachedFetch("/edge?" + query);
		const filter_mode = (this.filters.mode && this.filters.mode[mode]) || false;
		const is_active = +info.weight > 5;
		const back_polyline = await L.polyline(points[0], {
			color: styling.transport[mode].color,
			opacity: 0.3,
			offset: 0,
			weight: this.line_scaling(+info.size),
			info: {
				origin,
				target,
				mode,
				weight: +info.size,
				standby: true,
			},
		}).bindPopup(
			this.createEdgePopup({ origin, target, info, mode: info.type })
		);

		if (!filter_mode) back_polyline.addTo(this.map);
		if (!(mode.toLowerCase() in this.polylines)) {
			this.polylines[mode.toLowerCase()] = [];
		}
		this.polylines[mode.toLowerCase()].push(back_polyline);

		if (is_active) {
			const polyline = await L.polyline(points[0], {
				color: styling.transport[mode].color,
				opacity: 0.8,
				offset: 0,
				weight: this.line_scaling(+info.weight),
				info: {
					origin,
					target,
					mode,
					weight: +info.weight,
				},
			}).bindPopup(
				this.createEdgePopup({ origin, target, info, mode: info.type })
			);

			if (rightClick) {
				polyline.on("contextmenu", function (evt) {
					self.handleEdgeRightClick(polyline, evt);
				});
			}

			if (!filter_mode) polyline.addTo(this.map);
			this.polylines[mode.toLowerCase()].push(polyline);
		}
	}

	handleEdgeRightClick(polyline, evt) {}

	appendNodes() {
		this.nodes.forEach(this.appendNode);
	}

	async loadNodes() {
		await Promise.all(this.nodes.map(this.loadNode));
	}

	async loadNode({ name, properties }, node_id) {
		const query = new URLSearchParams({ name }).toString();
		const node = await this.cachedFetch("/node?" + query);
		node.properties = properties;
		this.nodes[node_id] = node;
	}

	async appendNode(node) {
		const marker = L.marker(node.coord, this.setNodeStyling(node)).bindPopup(
			this.createNodePopup(node)
		);
		marker.addTo(this.map);
		this.markers.push(marker);
	}

	setNodeStyling(node) {
		if (node.industry == "WtE") return this.setWtENodeStyling(node);
		return this.setTransitNodeStyling(node);
	}

	setTransitNodeStyling(node) {
		if (node.name == "Rekingen" || node.name == "Rothrist") {
			var icon = L.divIcon({
				html: "<div class='marker sbb' ></div>",
			});
		} else if (node.name == "Rotterdam") {
			var icon = L.divIcon({
				html: "<div class='marker port' ></div>",
			});
		} else if (node.industry == "Storage") {
			var icon = L.divIcon({
				html: "<div class='marker storage' ></div>",
			});
		} else {
			var icon = L.divIcon({
				html: "<div class='marker transit' ></div>",
			});
		}

		return { icon: icon };
	}

	getNodeEmissionsCaptured(node) {
		const index = this.filters.year - this.start_year;
		if (!("CCS_CO2" in node.properties)) return node.emittedCO2;
		return this.horizon == 1
			? node.properties.CCS_CO2
			: node.properties.CCS_CO2[index];
	}

	setWtENodeStyling(node) {
		const node_scaling = this.node_scaling;
		const technology = this.getNodeTechnology(node);
		const emissionsCaptured = this.getNodeEmissionsCaptured(node);
		const opacity = (emissionsCaptured / node.emittedCO2 / 0.9) * 0.8 + 0.2;
		const iconScale = node_scaling(node.emittedCO2);
		const style = `height:${iconScale * 12}px;width:${
			iconScale * 12
		}px;font-size:${iconScale * 5}px;line-height:${
			iconScale * 12
		}px;opacity:${opacity};`;
		var icon = L.divIcon({
			html: `<div class='marker ${technology}' style='${style}'>
				W
			</div>`,
		});
		return { icon: icon };
	}

	getNodeTechnology(node) {
		const props = node.properties;
		if ("CCS_CO2" in props && this.getPropValue(props.CCS_CO2) > 10) {
			return "ccs";
		} else if ("HPR" in props && this.getPropValue(props.HPR) > 2) {
			return "hpr";
		} else if ("PEMEC" in props && this.getPropValue(props.PEMEC) > 2) {
			return "pemec";
		} else if ("SMR" in props && this.getPropValue(props.SMR) > 2) {
			return "smr";
		} else if ("SMRCCS_PP" in props && this.getPropValue(props.SMRCCS_PP) > 2) {
			return "smrccs_pp";
		}
		return "";
	}

	formatTransportMode(mode) {
		switch (mode) {
			case "CO2R":
				return "train";
			case "CO2T":
				return "truck";
			case "CO2B":
				return "barge";
			case "CO2S":
				return "ship";
			case "CO2P_BF":
				return "pipeline";
			case "CO2P_GF":
				return "pipeline";
			case "woodT":
				return "woodT";
			case "hydrogenT":
				return "hydrogenT";
			default:
				return "default";
		}
	}

	createNodePopup(node) {
		const props = node.properties;
		if ("CCS_CO2" in props && this.getPropValue(props.CCS_CO2) > 100) {
			return this.createCCSPopup(node);
		}
		return this.createTransitPopup(node);
	}

	getPropValue(prop) {
		return typeof prop == "number" ? prop : Math.max(...prop);
	}

	createTransitPopup(node) {
		return () => {
			const flows = this.calculateFlows(node);
			const flowsHtml = this.formatFlows(flows);
			const industry = node.industry != "" ? node.industry : "Transit";
			return `<div class='popup'>
				<div class="title">
				${industry}: ${node.name}
				</div>
				<div class='body'>
				<ul class="flows">${flowsHtml}</ul>
				</div>
				</div>`;
		};
	}

	createGenericPopup(node) {
		return () => {
			const index = this.filters.year - this.start_year;

			return `<div class='popup'>
				<div class="title">
				${node.industry}: ${node.name}
				</div>
				<div class='body'>
				
				</div>
				</div>`;
		};
	}

	createCCSPopup(node) {
		return () => {
			const index = this.filters.year - this.start_year;
			const emissionsCaptured = this.getNodeEmissionsCaptured(node);
			const captureRatio = Math.round(
				(emissionsCaptured / node.emittedCO2) * 100
			);
			const flows = this.calculateFlows(node);
			const flowsHtml = this.formatFlows(flows);
			const captureHtml = this.formatCapture(emissionsCaptured, captureRatio);
			return `<div class='popup'>
				<div class="title">
				${node.industry}: ${node.name}
				</div>
				<div class='body'>
				<ul class="flows">${captureHtml}${flowsHtml}</ul>
				</div>
				</div>`;
		};
	}

	calculateFlows({ name }) {
		const flows = {
			in: [],
			out: [],
		};
		Object.entries(this.polylines).forEach(([mode, polys]) => {
			polys.forEach(({ options: { info } }) => {
				if (info.standby) {
					return;
				}
				if (info.target == name) {
					flows.in.push(info);
				}
				if (info.origin == name) {
					flows.out.push(info);
				}
			});
		});
		return flows;
	}

	formatCapture(emissionsCaptured, captureRatio) {
		const co2 = number_compact(emissionsCaptured);
		return `<li><span class="flow default">&darr;</span>${co2}tCO2 <em>(${captureRatio}%)</em> </li>`;
	}

	formatFlows(flows) {
		const inflows = flows.in
			.map((inflow) => {
				if (inflow.weight < 1) {
					return "";
				}
				const co2 = number_compact(inflow.weight);
				const classname = inflow.mode.toLowerCase();
				return `<li><span class="flow ${classname}">&larr;</span>${co2}tCO2 from ${inflow.origin}</li>`;
			})
			.join("");
		const outflows = flows.out
			.map((outflow) => {
				if (outflow.weight < 1) {
					return "";
				}
				const co2 = number_compact(outflow.weight);
				const classname = outflow.mode.toLowerCase();
				return `<li><span class="flow ${classname}">&rarr;</span>${co2}tCO2 to ${outflow.target}</li>`;
			})
			.join("");
		return `${inflows}${outflows}`;
	}

	createEdgePopup(edge) {
		const mode = edge.mode.slice(0, 1).toUpperCase() + edge.mode.slice(1);
		return (
			"<div class='popup'>" +
			mode +
			": " +
			number_compact(edge.info.weight) +
			" / " +
			number_compact(edge.info.size) +
			" tCO2,</br>" +
			"From " +
			edge.origin +
			" to " +
			edge.target +
			this.additionalModeInfo(mode, edge) +
			"</div>"
		);
	}

	additionalModeInfo(mode, edge) {
		if (mode == "Truck") {
			const ntrucks = Math.ceil(edge.info.weight / 20 / 364.25);
			return `<div class='info'>${ntrucks}<img class="icon" src="/static/res/icon/truck.png"></img> per day</div>`;
		}
		return "";
	}

	toggleMode(mode, state) {
		const map = this.map;
		if (!(mode in this.polylines)) return;
		this.polylines[mode].forEach((poly) => {
			if (state && !map.hasLayer(poly)) {
				poly.addTo(map);
			} else if (!state) {
				poly.remove(map);
			}
		});
	}

	setModeFilter(mode, state) {
		if (!this.filters.mode) this.filters.mode = {};
		this.filters.mode[mode] = !state;
	}

	clearModes() {
		const toggleMode = this.toggleMode;
		const modes = Object.keys(this.polylines);
		modes.forEach((mode) => {
			toggleMode(mode, false);
			this.polylines[mode] = [];
		});
	}

	clearMarkers() {
		const map = this.map;
		this.markers.forEach((marker) => {
			marker.remove(map);
		});
		this.markers = [];
	}

	appendLegend() {
		const self = this;
		const legend = document.createElement("ul");
		legend.classList.add("legend");
		const options = this.info.setNW
			.map(this.formatTransportMode)
			.map((nw) => nw.slice(0, 1).toUpperCase() + nw.slice(1));
		// const options = ["Truck", "Train", "Pipeline", "Barge", "Ship"];
		const nbrOfOptions = options.length;
		options.map((label, i) => {
			const value = label.toLowerCase();
			const optionItem = document.createElement("li");
			optionItem.classList.add("mode");
			const optionIcon = document.createElement("div");
			optionIcon.classList.add("line", value);
			const optionLabel = document.createElement("div");
			optionLabel.classList.add("btn");
			optionLabel.innerText = label;
			optionLabel.addEventListener("click", () => {
				const state =
					optionLabel.getAttribute("data-state") == "false" ? false : true;
				self.toggleMode(value, !state);
				self.setModeFilter(value, !state);
				optionLabel.setAttribute("data-state", !state);
			});
			optionItem.appendChild(optionIcon);
			optionItem.appendChild(optionLabel);
			legend.appendChild(optionItem);
		});
		const slider = document.createElement("li");
		const sliderLabel = document.createElement("label");
		sliderLabel.innerText = "Year";
		const sliderInput = document.createElement("input");
		sliderInput.classList.add("year-slider");
		sliderInput.setAttribute("type", "range");
		sliderInput.setAttribute("min", this.start_year);
		sliderInput.setAttribute("max", this.start_year + this.horizon - 1);
		sliderInput.setAttribute("value", this.start_year);
		const sliderValue = document.createElement("div");
		sliderValue.innerText = this.filters.year;
		sliderValue.classList.add("year-value");
		const updateYear = ({ target: { value } }) => {
			sliderInput.disabled = true;
			self.setFilters({ year: value });
			sliderValue.innerText = value;
			sliderInput.disabled = false;
		};
		sliderInput.addEventListener("mouseup", updateYear);
		sliderInput.addEventListener("touchend", updateYear);

		if (self.horizon > 1) {
			slider.appendChild(sliderLabel);
			slider.appendChild(sliderInput);
		}

		slider.appendChild(sliderValue);
		legend.appendChild(slider);
		document.getElementById(this.map_id).after(legend);
	}

	async notification(text, duration = 2000) {
		if (this.notif) {
			await this.removeNotification(this.notif);
		}
		const self = this;
		const notif = document.createElement("div");
		notif.classList.add("notification");
		notif.innerText = text;
		notif.addEventListener("click", function () {
			self.removeNotification(notif);
		});
		if (duration) {
			setTimeout(() => {
				self.removeNotification(notif);
			}, duration);
		}
		this.notif = notif;
		document.getElementById(this.map_id).append(notif);
	}

	removeNotification(notif) {
		return new Promise((res, rej) => {
			notif.classList.add("out");
			setTimeout(() => {
				notif.remove();
				res("done");
			}, 800);
		});
	}
}
