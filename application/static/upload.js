const title = document.querySelector("#title");
const file_upload = document.querySelector("#file_upload");
const drop_zone = document.querySelector("#drop_zone");
const submit = document.querySelector("#submit");
const upload_id =
	Math.random().toString(36).substring(2) +
	Math.random().toString(36).substring(2);

function dragOverHandler(ev) {
	ev.preventDefault();
}

function dragEnterHandler(ev) {
	drop_zone.querySelector(".off").style = "display:none;";
	drop_zone.querySelector(".on").style = "";
	ev.preventDefault();
}

function dragLeaveHandler(ev) {
	drop_zone.querySelector(".on").style = "display:none;";
	drop_zone.querySelector(".off").style = "";
	ev.preventDefault();
}

function dropHandler(ev) {
	ev.preventDefault();
	if (ev.dataTransfer.items && ev.dataTransfer.items.length == 1) {
		if (ev.dataTransfer.items[0].kind === "file") {
			const file = ev.dataTransfer.items[0].getAsFile();
			if (file.name.includes(".mat") || file.name.includes(".zip")) {
				uploadFile(file);
				return;
			}
		}
	}
	resetZone();
}

function uploadFile(file) {
	const uri = "/api/file/" + upload_id;
	const xhr = new XMLHttpRequest();
	const fd = new FormData();

	xhr.open("POST", uri, true);
	xhr.onreadystatechange = function () {
		if (xhr.readyState == 4 && xhr.status == 200) {
			const resp = JSON.parse(xhr.responseText);
			if (resp.valid) {
				appendFile(resp);
			} else {
				resetZone();
			}
		}
	};
	fd.append("file", file);
	xhr.send(fd);
}

function appendFile(resp) {
	file_upload.innerHTML = `<div> <file>${resp.file.ext}</file> uploaded</div> `;
	document
		.querySelector("input[name='filename']")
		.setAttribute("value", resp.file.filename);
}

function resetZone() {
	drop_zone.querySelector(".on").style = "display:none;";
	drop_zone.querySelector(".off").style = "";
}

drop_zone.addEventListener("dragover", dragOverHandler);
drop_zone.addEventListener("dragenter", dragEnterHandler);
drop_zone.addEventListener("dragleave", dragLeaveHandler);
drop_zone.addEventListener("drop", dropHandler);
