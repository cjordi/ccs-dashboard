from . import db
import polyline as pl
from flask_login import UserMixin


class User(UserMixin, db.Model):
    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(100))
    username = db.Column(db.String(100))
    name = db.Column(db.String(100))
    access = db.Column(db.Integer, default=0)


class Result(db.Model):
    __tablename__ = "results"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    rid = db.Column(db.String(22))
    rext = db.Column(db.String(3))
    description = db.Column(db.Text, default="")
    date = db.Column(db.Text, default="")
    uploaded = db.Column(db.Text)
    author = db.Column(db.String(100))

    def json(self):
        return {
            "id": self.id,
            "name": self.name,
            "rid": self.rid,
            "rext": self.rext,
            "description": self.description,
            "date": self.date,
            "uploaded": self.uploaded,
            "author": self.author
        }


class Node(db.Model):
    __tablename__ = "nodes"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    industry = db.Column(db.String(100))
    emittedCO2 = db.Column(db.Float)
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)

    def __repr__(self):
        return f'{"{"}"name":"{self.name}", "industry":"{self.industry}", "emittedCO2":{self.emittedCO2}, "latitude":{self.latitude}, "longitude":{self.longitude}{"}"}'


class Edge(db.Model):
    __tablename__ = "edges"
    id = db.Column(db.Integer, primary_key=True)
    origin = db.Column(db.String(100))
    target = db.Column(db.String(100))
    directed = db.Column(db.Boolean, default=0)
    mode = db.Column(db.String(100))
    polyline = db.Column(db.Text)
    parent = db.Column(db.Integer, default=None)

    def __repr__(self):
        return f"{self.origin} {(1^self.directed)*'<'}-- {self.mode} --> {self.target}"

    def data(self):
        return pl.decode(self.polyline)
