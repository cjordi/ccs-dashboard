
COST_FIELDS = {"total_costs", "investment_costs", "investment_costsY", "operation_costs",
               "operation_costsY", "LCOC"}
EMISSION_FIELDS = {"emissions", "emissionsY",
                   "emissionsCaptured", "emissionsCapturedPerNode"}
OPTI_FIELDS = {"objective"}
OPTI_SUBFIELDS = {"analysis.object": "task"}
GLOBAL_SUBFIELDS = {"analysis.year": "year",
                    "horizon.number": "horizon", "analysis.CO2tax": "CO2tax", "analysis.sets.setNW": "setNW"}

ALL_FIELDS = set.union(COST_FIELDS, EMISSION_FIELDS, OPTI_FIELDS)
ALL_SUBFIELDS = dict(GLOBAL_SUBFIELDS, **OPTI_SUBFIELDS)
BRIEF_FIELDS = set.union(COST_FIELDS, EMISSION_FIELDS)
BRIEF_SUBFIELDS = dict(GLOBAL_SUBFIELDS)
