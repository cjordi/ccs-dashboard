import os
import glob
import json
import shutil
import zipfile
import scipy.io
import shortuuid
import functools
import numpy as np
from pathlib import Path
from flask import current_app
from datetime import datetime

from .models import db, Result
from .fields import ALL_FIELDS, ALL_SUBFIELDS


def add(filename: str, title: str = "Untitled", author=None):
    """
    Adds a result to the database

    """
    rpath = os.path.join(current_app.config['UPLOAD_FOLDER'], filename)
    rid, rext = os.path.splitext(filename)
    upload_date = datetime.now().isoformat()
    new_result = Result(name=title, rid=rid, rext=rext,
                        uploaded=upload_date, author=author)
    db.session.add(new_result)
    db.session.commit()


def get_all(full: bool = False):
    """
    Returns all the results

    Args:
        full (bool): if True loads all the data for a result

    Returns:
        {} or [{}]
    """
    results = Result.query.order_by(Result.id).all()
    output = []
    for result in results:
        if full:
            output.append(get(result.id))
        else:
            output.append({
                "id": result.id,
                "name": result.name,
                "date": result.date,
            })
    return output[::-1]


def get(dbid: int):
    """
    Returns the result
    """
    result = Result.query.get(dbid)
    if result:
        return load(result)
    return None


def get_name(dbid: int):
    """
    Returns the result name
    """
    result = Result.query.get(dbid)
    if result:
        return result.name
    return ""


def load(result):
    """
    Load the result
    """
    rdata = result.json()
    rext = result.rext
    rid = result.rid
    rpath = os.path.join(current_app.config["UPLOAD_FOLDER"], rid)
    if rext == ".mat":
        rdata["data"] = load_mat(rpath)
    elif rext == ".zip":
        rdata["data"] = load_zip(rpath)
    return rdata


def load_mat(rpath: str):
    if ".mat" not in rpath:
        rpath += ".mat"
    data = {}
    try:
        struct = scipy.io.loadmat(rpath, squeeze_me=True)["object"]
        for field in ALL_FIELDS:
            val = np.atleast_1d(struct[field]).squeeze()
            data[field] = val.tolist()
        for sub, field in ALL_SUBFIELDS.items():
            loc = struct
            for child in sub.split(".")[:-1]:
                loc = loc[child][()]
            val = np.atleast_1d(loc[sub.split(".")[-1]]).squeeze()
            data[field] = val.tolist()
    except:
        pass
    return data


def to_str(items):
    if type(items) == str:
        return items
    if type(items) == float or type(items) == int:
        return "{:.2e}".format(items)
    return ["{:.2e}".format(i) for i in items if type(i) != str]


def load_zip(fpath: str):
    if not os.path.isdir(fpath + "/"):
        unzip(fpath)
    base_path = os.path.join(fpath, 'base')
    return load_mat(base_path)


def unzip(fpath: str):
    """
    Unzip a zip file
    """
    with zipfile.ZipFile(fpath + ".zip", 'r') as zip_ref:
        zip_ref.extractall(fpath + "/")


def set_description(dbid: int, description: str):
    result = Result.query.get(dbid)
    result.description = description
    db.session.commit()
    return "200"


def set_name(dbid: int, name: str):
    result = Result.query.get(dbid)
    result.name = name
    db.session.commit()
    return "200"


def delete(dbid: int):
    """
    Delete a result
    """
    result = Result.query.get(dbid)
    rext = result.rext
    rid = result.rid
    rpath = os.path.join(
        current_app.config["UPLOAD_FOLDER"], rid)

    if rext == ".mat":
        if os.path.exists(rpath + ".mat"):
            os.remove(rpath + ".mat")
    elif rext == ".zip":
        if os.path.isdir(rpath + "/"):
            shutil.rmtree(rpath + "/")
        if os.path.exists(rpath + ".zip"):
            os.remove(rpath + ".zip")

    db.session.delete(result)
    db.session.commit()
