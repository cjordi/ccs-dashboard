import os
import json
import polyline
from . import results as Results
from . import network as Network
from .render import render_result
from .models import db, Edge, Node
from flask_login import current_user, login_required
from flask import Blueprint, current_app, render_template, flash, request, url_for, redirect,  jsonify

main = Blueprint('main', __name__)


@main.route('/')
@login_required
def index():
    results = Results.get_all(full=True)
    return render_template("all.html", highlight="results", results=results)


@main.route('/upload', methods=['GET'])
@login_required
def get_upload():
    results = Results.get_all()
    return render_template("upload.html", highlight="upload", results=results)


@main.route('/upload', methods=["POST"])
@login_required
def post_upload():
    filename = request.form.get("filename")
    title = request.form.get("title")
    author = current_user.username
    Results.add(filename, title, author)
    return redirect(url_for("main.index"))


@main.route("/results/<result_id>")
@login_required
def get_result(result_id):
    results = Results.get_all()
    result = Results.get(result_id)
    if result:
        return render_result(result=result, results=results)
    return redirect(url_for("main.index"))


@main.route("/network/<result_id>")
def get_network(result_id):
    result = Results.get(result_id)
    path = os.path.join(
        current_app.config["UPLOAD_FOLDER"], result["rid"])
    if result["rext"] == ".zip":
        network = Network.get_from_folder(path)
    else:
        network = Network.get(path)
    return jsonify(network)


@main.route("/node")
def get_node():
    name = request.args.get("name")
    node = Node.query.filter_by(name=name).first()
    if node:
        data = json.loads(node.__repr__())
        data["coord"] = [data["latitude"], data["longitude"]]
        return jsonify(data)
    return jsonify({})


@main.route("/edge")
def get_edge():
    origin = request.args.get("origin")
    target = request.args.get("target")
    mode = request.args.get("mode")
    edge = Network.load_edge(origin, target, mode)
    return jsonify(edge)
