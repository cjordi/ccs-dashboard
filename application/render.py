import numpy as np
from flask import Blueprint, current_app, render_template, flash, request, url_for, redirect,  jsonify


def render_result(result: object, results: list):
    """
    Render html page for a given result
    """
    result = expand(result)
    if result["rext"] == ".zip":
        return render_template("risk.html", highlight="results", result=result, results=results)

    return render_template("result.html", highlight="results", result=result, results=results)


def expand(result):
    """
    Expand the info for a given result
    """
    result["data"]["start"] = int(result["data"]["year"])
    result["data"]["end"] = result["data"]["start"] + \
        result["data"]["horizon"] - 1
    result["data"]["task"] = format_task(result["data"]["task"])
    result["data"]["compact"] = {
        "emissions": number_compact(result["data"]["emissions"]),
        "LCOC": number_compact(result["data"]["LCOC"]),
        "total_costs": number_compact(result["data"]["total_costs"]),
        "emissions_captured": number_compact(np.sum(result["data"]["emissionsCaptured"]).tolist())
    }
    return result


def mod_sum(data):
    if type(data) == list:
        return sum(data)
    return data


def format_task(task: int):
    if task == 0:
        return 'unset'
    if task == 1:
        return 'min costs'
    if task == 2:
        return 'min CO2'
    if task == 3:
        return 'multi-objective'
    if task == 4:
        return 'pareto front'
    if task == 5:
        return 'epsilon constraint'
    if task == 7:
        return "N-1"


def number_compact(number):
    if type(number) != int and type(number) != float:
        return "None"
    if number > 1e9:
        bils = number/1e9
        bils = int(bils)
        return f"{bils} B"
    if number > 1e6:
        mils = number/1e6
        mils = int(mils)
        return f"{mils} M"
    if number > 1e3:
        thds = number/1e3
        thds = int(thds)
        return f"{thds} k"
    number = int(number)
    return f"{number}"


def number_string(data):
    for field in data:
        if type(data[field]) is int or type(data[field]) is float:
            data[field] = "{:.2e}".format(data[field])
    return data
