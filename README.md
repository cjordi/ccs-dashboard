# CCS Dashboard

Simple and extensible dashboard prototype to visualize carbon capture & storage (CCS) network topologies and export svg network visualizations.

The dashboard was developed in parrallel of my Masters Thesis at the Reliability and Risk Engineering (RRE) lab at ETHZ. The dashboard running at https://ccus.ch is currently being utilized by the RRE and Seperation Process Lab (SPL) at ETHZ for their visualization and graphical output generation tasks.

## Preview

![ccus.ch demo](./ccus_demo.gif)

## Installation

Create a virtual environment for python:

```bash
virtualenv venv
```

Open the environment:

```bash
source venv/bin/activate
```

Install the requirements:

```bash
pip install -r requirements.txt
```

Run the dashboard:

```bash
python3 app.py
```

To view the dashboard, open the displayed address in your web browser.

To stop the dashboard, press `CTRL+C` in terminal.

To exit the virtual environment:

```bash
deactivate
```
